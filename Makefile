all: test install clean

install:
	@pip install -e .

test:
	@python -m pytest --disable-warnings \
		--testdox \
		--cov=. tests.py

clean:
	@python setup.py clean
	@rm -rf *.egg-info dist build

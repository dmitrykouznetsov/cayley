#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2019 Dmitry Kouznetsov <dmitry.kouznetsov@protonmail.com>
#
# Cayley is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 2 of the
# License, or (at your option) any later version.
#
# Cayley is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see http://www.gnu.org/licenses/.
#
import click
import os, sys
from neo4j import GraphDatabase
import hashlib
import shutil
import pprint
from typing import Iterable

PLACEHOLDER = "%n"


class ExitCode:
    """The errors are divided into 3 categories (errorcodes) depending on the
    level at which they occur."""
    LocalError = 1
    ServerError = 2
    QueryError = 3


class QueryError:
    """These errors deal with invalid or dangerous queries"""
    NoPlaceholder = f"The cypher must contain the placeholder {PLACEHOLDER} for the neo4j node created from the file"


class Metadata:
    """Record of relevant metadata of a file.

    The main purpose of this class is to aggregate the important bits of
    metadata of a file and return a formatted string with all of this info
    in the form of a neo4j `:File` node.
    
    Args:
        file: Name of the file

    Attributes:
        sha1: The unique(*) SHA1 hash code of the file
        unixtime: Number of seconds elapsed since the Unix Epoch
        filesize: Size of the file in bytes
        extension: File extension with prepended dot
        filename: The file is named with its sha1 hash, maintaining the extension
        
    """
    def __init__(self, file: str):
        self.sha1 = calculate_hash(file)
        self.unixtime = int(os.path.getctime(file))
        self.filesize = os.path.getsize(file)
        ext = os.path.splitext(file)[1]
        self.extension = ext if ext else ""

    def __str__(self):
        return "(:File {{sha1: '{}', unixtime: '{}', filesize: '{}', extension: '{}'}})".format(
            self.sha1, self.unixtime, self.filesize, self.extension)

    @property
    def filename(self):
        return self.sha1 + self.extension


def calculate_hash(file: str) -> str:
    """Create SHA1 hash object from the contents of a file
    
    Args:
        file: Name of the file
        
    Returns:
        (str) The hash object digest is returned as a string object of double
        length, containing only hexadecimal digits. This may be used to
        exchange the value safely in email or other non-binary environments.

    """
    sha1sum = hashlib.sha1()
    path = os.path.abspath(file)

    with open(path, 'rb') as source:
        block = source.read(2**16)
        while len(block) != 0:
            sha1sum.update(block)
            block = source.read(2**16)

    return sha1sum.hexdigest()


def send_neo4j(server: str, username: str, password: str, query: str) -> dict:
    """Execute a cypher statement over the bolt protocol
    
    Args:
        server: Neo4j server address
        user: Noe4j username
        password: Neo4j password
        query: Cypher query

    Returns;
        (dict): Stream of records resulting from the cypher query

    """
    driver = GraphDatabase.driver(server, auth=(username, password))

    with driver.session() as session:
        result = session.write_transaction(lambda tx: tx.run(query))

    return result.__dict__


@click.command()
@click.argument("cypher", required=False)
@click.option("--file", help="File to be added as a node in neo4j")
@click.option(
    "--target",
    envvar="REMOTE_DIR",
    help="Target directory where the file will be moved (env REMOTE_DIR)")
@click.option("--username",
              default="neo4j",
              help="The user account for access to neo4j (default: neo4j)")
@click.option("--password",
              envvar="NEO4J_PASSWORD",
              help="The authentication password for neo4j (env NEO4J_PASSWORD)"
              )
@click.option("--server",
              default="bolt://localhost:7687",
              help="Neo4j server address (default: bolt://localhost:7687)")
def cli(cypher, file, target, username, password, server):
    if not cypher:

        # The script should behave like a unix command line tool and
        # accept a pipe or a filename as argument
        try:
            cypher = sys.stdin.read()

        # This workflow is broken on windows
        except Exception as e:
            print(e)
            sys.exit(ExitCode.LocalError)

    # Stop execution if cypher has no %n placeholder
    if not PLACEHOLDER in cypher:
        print(QueryError.NoPlaceholder)
        sys.exit(ExitCode.QueryError)

    try:
        metadata = Metadata(file)
        result = send_neo4j(server, username, password,
                            cypher.replace(PLACEHOLDER, str(metadata)))

        # Put to stdout the results from the cypher query
        pp = pprint.PrettyPrinter(indent=1)
        pp.pprint(result)

    except Exception as e:
        print(e)
        sys.exit(ExitCode.ServerError)

    try:
        shutil.move(file, os.path.join(target, metadata.filename))

    except Exception as e:
        print(e)
        sys.exit(ExitCode.LocalError)

import pytest
from click.testing import CliRunner
from cayley import calculate_hash, cli, QueryError, ExitCode


@pytest.fixture
def default_options():
    return [
        "--username", "neo4j", "--password", "dummypassword", "--server",
        "bolt://localhost:8888"
    ]


def test_hash():
    assert calculate_hash("testfile") == "da39a3ee5e6b4b0d3255bfef95601890afd80709", \
            "SHA1 should match"


def test_bad_query(default_options):
    result = CliRunner().invoke(
        cli, default_options + ["--file", "testfile", "CREATE"])
    assert result.exit_code == ExitCode.QueryError
    assert result.output == QueryError.NoPlaceholder + "\n"


def test_no_connection(default_options):
    result = CliRunner().invoke(
        cli, default_options + ["--file", "testfile", "CREATE %n"])
    assert result.exit_code == ExitCode.ServerError
    assert result.output == "Failed to establish connection to ('127.0.0.1', 8888) (reason [Errno 111] Connection refused)\n"

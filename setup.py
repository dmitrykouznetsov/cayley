from setuptools import setup

setup(
    name="cayley",
    version="0.1",
    description="Tool to move file to a directory and send metadata to Neo4j",
    url="https://github.com/dmitrykouznetsov/cayley",
    author="Dmitry Kouznetsov",
    license="GPLv2",
    packages=[],
    install_requires=["click", "neo4j"],

    # The extras can be installed with `pip install -e .[test]`
    extras_require={

        # Less testing overload by checking types
        "test": ["mypy", "pytest", "pytest-cov", "pytest-testdox"],
    },

    # This part is specific for the click library
    entry_points="""
    [console_scripts]
    cayley=cayley:cli
    """,

    # Slight performance boost
    zip_safe=True)
